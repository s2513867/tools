# Tools
This is a repository that contains some of my scripts, snippets and tools that I created to assist my study in this course.

## Description
file(s) | description | instruction
-- | -- | --
ManageDownloads.sh | This is a simple shell script that moves all files and directories (excluding hidden files such as `.bash_profile` or `.vim`)  from `/home/m23oc/m23oc/$USERNAME/` to `/work/m23oc/m23oc/$USERNAME` | Put it under `/work/m23oc/m23oc/$USERNAME/` and run `./ManageDownloads.sh`