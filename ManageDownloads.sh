#!/bin/bash
set -e
# Assume this file sits in /work/m23oc/m23oc/s1234567/. If not, please set your STUDENT_ID manually here.
STUDENT_ID=$(basename "$PWD")
mv /home/m23oc/m23oc/$STUDENT_ID/** /work/m23oc/m23oc/$STUDENT_ID/Downloads/
echo "Moved all files from home directory to work directory!"